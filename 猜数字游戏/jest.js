// __tests__/sum-test.js
jest.dontMock('package/index.js');

describe('sum', function() {
 it('adds 1 + 2 to equal 3', function() {
   var sum = require('package/index.js');
   expect(sum(1, 2)).toBe(3);
 });
});