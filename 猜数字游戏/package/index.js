new Vue({
    el:'#app',
    data() {
        return {
            history:[],
            activeNames: [''],//折叠面板
            answer:'', // 随机生成的答案
            buttons:[],//100个按钮
            arr:[],//100个数字集合 位置随机排序
            color:'',//按钮颜色
            right:false,
            message:'',//大小提示
            startGame:true,//游戏开始标志
            choose:0,
            loss:false, // 失败
            config:{},  //配置文件
            numberOrder:'',//
            answerOpen:'',//随机答案
        }
    },
    created() {

        // 读取配置文件
        if(!JSON.parse(cookie.get('config'))){
            this.config = {
                numberOrder:true,//数字打乱顺序
                answerOpen:false,//随机答案 默认关闭
            }
            cookie.set( 'config' , JSON.stringify( this.config ) , 3650 )
        }

        this.config = JSON.parse(cookie.get('config'))
        this.numberOrder = this.config.numberOrder
        this.answerOpen = this.config.answerOpen
        this.history = JSON.parse(cookie.get('history'))

        // 游戏开始
        this.gameAction()
    },
    watch: {
      numberOrder(newV,oldV){
        this.config = {
            numberOrder: newV,//数字打乱顺序
            answerOpen: this.numberOrder,//随机答案 默认关闭
        }
        const that = this
        setTimeout(function(){
            that.$notify({
                title: '成功',
                message:this.numberOrder? '数字顺序已经打乱': '数字顺序已经恢复',
                type: 'success'
            });
        },0)
        cookie.set('config', JSON.stringify( this.config ), 3650 )
      },
      answerOpen(newV,oldV){
        this.config = {
            numberOrder: this.numberOrder,//数字打乱顺序
            answerOpen: newV ,//随机答案 默认关闭
        }
        const that = this
        setTimeout(function(){
            that.$notify({
                title: '成功',
                message: this.answerOpen? '打开随机答案'+this.answer: '关闭随机答案'+this.answer ,
                type: 'success'
              });
        },0)
        cookie.set('config', JSON.stringify( this.config ), 3650 )
      }
      
    },
    methods: {
        cookieSet(click,success){
            // 保存记录 
            const time = new Date()
            const his = {
                time: time,
                click:click,
                success:success
            }
            this.history.unshift(his)
            cookie.set("history", JSON.stringify( this.history), 3650);
        },
        gameAction(){
            this.arr = this.start()
            console.log(this.arr)
            if(JSON.parse(cookie.get('config')).answerOpen){
                this.answer = JSON.stringify(Math.random()*100+1).split('.')[0]
            }else{
                this.answer = 50
            }
        },
        /**
         * 点击 button
         */
        handleButton(item,index){
            console.log(item.num,index)
            
            this.choose++
            if(this.choose<7){
                // 选择对了
                if(this.answer === item.num){
                    this.right = true
                    return   this.$message({
                        message: '恭喜你选对了' ,
                        type: 'success'
                    });
                }
                this.buttons[index].disabled = true

                // 判断 是大 还是小 了 
                if(this.buttons[index].num > this.answer ){
                    this.message = '你这个大了' 
                }
                if(this.buttons[index].num < this.answer ){
                    this.message = '你这个小了' 
                }
                // 提示 猜数字 错误
                this.$message({
                    message:  this.message,
                    type: 'error'
                });
                // console.log( document.querySelectorAll('.el-button')[index]) 
            }else{
                this.loss = true
                for(let i = 0 ; i < 100;i++  ){
                    this.buttons[i].disabled = true
                }
                // 保存记录 
                this.cookieSet(7,false)
            }
        },
        // 重置 游戏 
        reset(){
            this.buttons = []
            this.start()
            this.loss = false
            this.choose = 0
        },
        start(){
            // 创建 1-100 arr 
            for(let i=1;i<=100;i++){
                this.arr.push(i)
            }

            if(this.numberOrder){
                // 排序
                var randomNumber = function(){
                    // randomNumber(a,b) 返回的值大于 0 ，则 b 在 a 的前边；
                    // randomNumber(a,b) 返回的值等于 0 ，则a 、b 位置保持不变；
                    // randomNumber(a,b) 返回的值小于 0 ，则 a 在 b 的前边。
                    return 0.5 - Math.random()
                }
                this.arr.sort(randomNumber)
            }
            // 生成一百个按钮 并且 初始化属性
            for(let i =0 ;i<100;i++){
                let a = {
                    index : i, //序号
                    num   : this.arr[i],//按钮背后的数字
                    color:"",
                    disabled:false,
                }
                this.buttons.push(a)
            }
            return  this.arr
        },
        // 折叠面板
        handleChange(val) {
            // console.log(val);
        }
    },
})